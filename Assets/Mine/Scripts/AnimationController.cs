﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {
    [SerializeField]
    float jumpForce;
    Animator anim;
    Rigidbody rb;
    bool grounded, attacking;
    // Use this for initialization
    void Start() {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("Grounded", grounded);
        if (Input.GetKeyDown(KeyCode.G) && attacking == false) {
            anim.SetTrigger("Kick");
            attacking = true;
        }
        if (Input.GetKeyDown(KeyCode.F) && attacking == false) { 
        anim.SetTrigger("Punch");
            attacking = true;
        }
        if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
        {
            anim.SetBool("Running", true);
        }
        else
        {
            anim.SetBool("Running", false);
        }
        if (grounded){
            if (Input.GetKeyDown(KeyCode.Space)){
                anim.SetTrigger("Jump");
                }
            } 
    }
        void OnCollisionStay(Collision coll)
        {
            if (coll.gameObject.tag == "Grounded")
            {
                grounded = true;
            }
            else
            {
                grounded = false;
            }

        }
    private void OnCollisionExit(Collision drt)
    {
if (drt.gameObject.tag == "Grounded"){
            grounded = false;
        }
    }
    void ActualJump(){
        print("jumper");
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
       }
    } 


