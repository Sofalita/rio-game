﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtachedArrow : MonoBehaviour {
    [SerializeField] float life;
	// Use this for initialization
	void Start () {
        Transform part = transform.parent.transform;
        transform.SetParent(null);
        transform.localScale = new Vector3(1, 1, 1);
        transform.SetParent(part);
	}
	
	// Update is called once per frame
	void Update () {
        life -= Time.deltaTime;
        if (life <= 0)
        {
            Destroy(gameObject);
        }
    }
}
